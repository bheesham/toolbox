; Toolbox
; Copyright (C) 2015 Bheesham Persaud and Daniel Silverthorn.
(ns toolbox.security
  (:import
    (javax.crypto
      Cipher)
    (java.security
      KeyFactory)
    (java.security.spec
      KeySpec
      PKCS8EncodedKeySpec X509EncodedKeySpec
      InvalidKeySpecException)
    (org.apache.commons.io IOUtils))
  (:require
    [clojure.java.io :as io]
    [toolbox.core :refer :all]))

(def cipher (atom (Cipher/getInstance "RSA/ECB/PKCS1Padding")))

(defn- read-private [file]
  (.generatePrivate (KeyFactory/getInstance "RSA")
    (new PKCS8EncodedKeySpec file)))

(defn- read-public [file]
  (.generatePublic (KeyFactory/getInstance "RSA")
    (new X509EncodedKeySpec file)))

(defn- read-raw
  "Reads from file into a byte array."
  [file]
  (IOUtils/toByteArray (io/input-stream file)))

(defn read-key
  "Reads from a keyfile, returns nil if unsuccessful."
  [file]
  (let [handle (read-raw file)]
    (cond
      (.endsWith file "pub") (read-public handle)
      (.endsWith file "pri") (read-private handle)
      :else nil)))

(defn encrypt [public, message]
  (print "encrypting message with public-key"))

(defn decrypt [private-key, message]
  (println "decrypting message with private-key"))

(defn sign [private-key, message]
  (println "signing message with private-key"))

(defn verify [public-key, message]
  (println "verifying signature with public-key"))
