(defproject toolbox "0.1.0-SNAPSHOT"
  :description "A Clojure library for use with Hammer and Nail."
  :url "https://bitbucket.org/bheesham/toolbox"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [
    [org.clojure/clojure "1.6.0"]
    [commons-io "1.3.2"]])
