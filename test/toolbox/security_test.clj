; Toolbox
; Copyright (C) 2015 Bheesham Persaud and Daniel Silverthorn.
(ns toolbox.security-test
  (:require [clojure.test :refer :all]
            [toolbox.security :as sec]))

; Test private key 
(deftest a-read-private
  (testing "reading private key"
    (is (= false (nil? (sec/read-key "test/test.pri"))))))

; Test public key 
(deftest a-read-public
  (testing "reading public key"
    (is (= false (nil? (sec/read-key "test/test.pub"))))))

(deftest a-encrypt
  (testing "encrypting with key"
    (is (= 1 1))))

(deftest a-decrypt
  (testing "decrypting with key"
    (is (= 1 1))))

(deftest a-sign
  (testing "signing with key"
    (is (= 1 1))))

(deftest a-verify
  (testing "verifying signature"
    (is (= 1 1))))
